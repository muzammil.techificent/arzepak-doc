# [MyPropertyActivity](../../app/src/main/java/com/alnafay/arzepak/activities/myProperty/MyPropertyActivity.kt)
## Introduction
This class shows User properties along their statuses .
## Responsibilitydocs/activity/WebActivity.md
- Shows user properties
- Delete property
- Edit property
- Hide property
- Un-Hide property
## Parent Class
[BaseDrawerActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/navDrawerActivity/BaseDrawerActivity.kt)
## Presenter
[MyPropertyPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/myProperty/MyPropertyPresenter.kt)
## View
[MyPropertyView](../../app/src/main/java/com/alnafay/arzepak/activities/myProperty/MyPropertyView.kt)
## Model
[Property](../../app/src/main/java/com/alnafay/arzepak/model/propertiesListing/Property.kt)
## Navigation
- **Navigate to**
  - [AddProperty](../../docs/activity/AddProperty.md)
- **Navigate from**
  - [BaseDrawerActivity](../../docs/base/BaseDrawerActivity.md)