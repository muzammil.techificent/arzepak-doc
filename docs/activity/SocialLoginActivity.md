# [SocialLoginActivity](../../app/src/main/java/com/alnafay/arzepak/activities/login/socialLogin/SocialLoginActivity.kt)
## Introduction
Activities which are using Drawer will extend this class .
## Responsibility
This class is responsible to navigate new screens . Can create custom UI in this class
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[SocialLoginPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/login/socialLogin/SocialLoginPresenter.kt)
## View
[SocialLoginViewInterface](../../app/src/main/java/com/alnafay/arzepak/activities/login/socialLogin/SocialLoginViewInterface.kt)
## Navigation
- **Navigate to**
  - Main Screen
  - Phone number screen
- **Navigate from**
  - drawer
  - Dialog
