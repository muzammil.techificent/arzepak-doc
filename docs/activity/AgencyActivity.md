# [AgencyActivity](../../app/src/main/java/com/alnafay/arzepak/activities/agency/AgencyActivity.kt)
## Introduction
Get Agency detail from server and show to the fields .
## Responsibility
- Get Agency detail
- Get property added by the agency
- Can call the agency by the given number .
## Parent Class

[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[AgencyPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/agency/AgencyPresenter.kt)
## View
[AgencyView](../../app/src/main/java/com/alnafay/arzepak/activities/agency/AgencyView.kt)
## Model
[Property](../../app/src/main/java/com/alnafay/arzepak/model/propertiesListing/Property.kt)
[FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
[Agency](../../app/src/main/java/com/alnafay/arzepak/model/agency/Agency.kt)
[Attachment](../../app/src/main/java/com/alnafay/arzepak/model/Attachment.kt)
## Navigation
- **Navigate to**
  - [UserPropertyActivity](../../docs/activity/UserPropertyActivity.md)
- **Navigate from**
  - [AgenciesActivity](../../docs/activity/AgenciesActivity.md)
  - [NewDiscoverFragment](../../docs/fragments/NewDiscoverFragment.md)
  - [AgencyFavouriteFragment](../fragments/AgencyFavouriteFragment.md)
