# [SelectAmenitiesActivity](../../app/src/main/java/com/alnafay/arzepak/activities/selectAmenities/SelectAmenitiesActivity.kt)
## Introduction
Amenities are  desirable or useful features or facilities of a building or place. This class will get these amenities and shows then in list format .
## Responsibility
- Get Amenities
- Select Amenities ( can be more then one )
- Send these amenities back where this class is called from .
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[SelectAmenitiesPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/selectAmenities/SelectAmenitiesPresenter.kt)
## View
[SelectAmenitiesView](../../app/src/main/java/com/alnafay/arzepak/activities/selectAmenities/SelectAmenitiesView.kt)
## Model
[Amenity](../../app/src/main/java/com/alnafay/arzepak/model/config/Amenity.kt)
## Navigation
- **Navigate to**
  - [AddPropertyFragmentStep2](../../docs/fragments/AddPropertyFragmentStep2.md)
- **Navigate from**
  - [AddPropertyFragmentStep2](../../docs/fragments/AddPropertyFragmentStep2.md)