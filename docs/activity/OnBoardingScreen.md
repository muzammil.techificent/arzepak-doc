# [OnBoardActivity](../../app/src/main/java/com/alnafay/arzepak/activities/onBoarding/OnBoardActivity.kt)
## Introduction
This class will show tutorials .
## Responsibility
- Shows tutorials
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[OnBoardPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/onBoarding/OnBoardPresenter.kt)
## View
[OnBoardViewInterface](../../app/src/main/java/com/alnafay/arzepak/activities/onBoarding/OnBoardViewInterface.kt)
## Navigation
- **Navigate to**
  - [SocialLoginActivity](../../docs/activity/SocialLoginActivity.md)
  - [HomeActivity](../../docs/activity/HomeActivity.md)
- **Navigate from**
  - [SplashActivity](../../docs/activity/SplashActivity.md)