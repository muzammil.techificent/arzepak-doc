# [PropertyDetailActivity](../../app/src/main/java/com/alnafay/arzepak/activities/propertyDetail/PropertyDetailActivity.kt)
## Introduction
This class will show Property detail which will be get by server on request .
## Responsibility
- Shows property detail including all params .
- Favourite property
- Share property
- Show video
- Display phone number of the owner of the property.
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[PropertyDetailPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/propertyDetail/PropertyDetailPresenter.kt)
## View
[PropertyDetailView](../../app/src/main/java/com/alnafay/arzepak/activities/propertyDetail/PropertyDetailView.kt)
## Model
- [Attachment](../../app/src/main/java/com/alnafay/arzepak/model/Attachment.kt)
- [Amenity](../../app/src/main/java/com/alnafay/arzepak/model/config/Amenity.kt)
- [FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
- [Property](../../app/src/main/java/com/alnafay/arzepak/model/propertiesListing/Property.kt)
## Navigation
- **Navigate to**
  - [VideoPlayerActivity](../../docs/activity/VideoPlayerActivity.md)
- **Navigate from**
  - [AgencyActivity](../../docs/activity/AgencyActivity.md)
  - [MyPropertyActivity](../../docs/activity/MyPropertyActivity.md)
  - [HomeFragment](../../docs/fragments/HomeFragment.md)
  - [NewDiscoverFragment](../../docs/fragments/NewDiscoverFragment.md)
  - [PropertiesListActivity](../../docs/activity/ProperiesListActivity.md)
  - [UserPropertyActivity](../../docs/activity/UserPropertyActivity.md)
  - [PropertyFavouriteFragment](../fragments/PropertyFavouriteFragment.md)

