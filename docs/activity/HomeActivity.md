# [HomeActivity](../../app/src/main/java/com/alnafay/arzepak/activities/home/HomeActivity.kt)
## Introduction
Main Screen after Authentication is Home Screen which consist of Drawer Screen .
## Responsibility
This class is responsible to show these fragments.
- [Home Fragment](../../docs/fragments/HomeFragment.md)
- [NewDiscoverFragment](../../docs/fragments/NewDiscoverFragment.md)
- [Favourite Fragment](../../docs/fragments/FavouriteFragment.md)
## Parent Class
[BaseDrawerActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/navDrawerActivity/BaseDrawerActivity.kt)
## Presenter
[HomePresenter](../../app/src/main/java/com/alnafay/arzepak/activities/home/HomePresenter.kt)
## View
[HomeViewInterface](../../app/src/main/java/com/alnafay/arzepak/activities/home/HomeViewInterface.kt)
## Navigation
- **Navigate from**
  - [SplashActivity](../../docs/activity/SplashActivity.md)
  - [OnBoardingScreen](../../docs/activity/OnBoardingScreen.md)
  - [SocialLoginActivity](../../docs/activity/ManualLoginActivity.md)
  - [SocialLoginActivity](../../docs/activity/SocialLoginActivity.md)
  - [ProfileCompleteActivity](../../docs/activity/ProfileCompleteActivity.md)
  - [Registration](../../docs/activity/Registration.md)
  - [BaseDrawerActivity](../../docs/base/BaseDrawerActivity.md)
- **Navigate to**
  - [SearchFilterActivity](../../docs/activity/SearchFilterActivity.md)
  - [SearchActivity](../../docs/activity/SearchActivity.md)
  - [PropertyDetailActivity](../../docs/activity/PropertyDetailActivity.md)
