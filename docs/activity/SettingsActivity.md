# [SettingsActivity](../../app/src/main/java/com/alnafay/arzepak/activities/settings/SettingsActivity.kt)
## Introduction
Activities which are using Drawer will extend this class .
## Responsibility
This class is responsible to navigate new screens . Can create custom UI in this class
## Parent Class

[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[SettingsPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/settings/SettingsPresenter.kt)
## View
[SettingsView](../../app/src/main/java/com/alnafay/arzepak/activities/settings/SettingsView.kt)
## Model
[AreaUnit](../../app/src/main/java/com/alnafay/arzepak/model/config/AreaUnit.kt)
## Navigation
- **Navigate to**
  - [UserEditProfileActivity](../../docs/activity/UserEditProfileActivity.md)
  - ChangePasswordActivity
- **Navigate from**
  - [BaseDrawerActivity](../../docs/base/BaseDrawerActivity.md)