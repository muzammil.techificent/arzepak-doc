# [SplashActivity](../../app/src/main/java/com/alnafay/arzepak/activities/splash/SplashActivity.kt)
## Introduction
This class appears at very first time whenever application is launched .
## Responsibility
- Stays for 5 Sec
- Check if user already Logged In then move to [HomeActivity](../../docs/activity/HomeActivity.md) otherwise move to [OnBoardingActivity](../../docs/activity/OnBoardingScreen.md).
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[SplashActivityPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/splash/SplashActivityPresenter.kt)
## View
[SplashActivityView](../../app/src/main/java/com/alnafay/arzepak/activities/splash/SplashActivityView.kt)
## Navigation
- **Navigate to**
  - [HomeActivity](../../docs/activity/HomeActivity.md)
  - [OnBoardingActivity](../../docs/activity/OnBoardingScreen.md)
