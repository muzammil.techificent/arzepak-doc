# [WebActivity](../../app/src/main/java/com/alnafay/arzepak/activities/web/WebActivity.kt)
## Introduction
This class will show you the web content that is given by back-end developer in form if link.
## Responsibility
- shows web content
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Navigation
- **Navigate from**
  - [BaseDrawerActivity](../../docs/base/BaseDrawerActivity.md)