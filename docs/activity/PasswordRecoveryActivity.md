# [PasswordRecoveryActivity](../../app/src/main/java/com/alnafay/arzepak/activities/passwordRecovery/PasswordRecoveryActivity.kt)
## Introduction
This class recover user password .
## Responsibility
- Take email
- Send to the server for password recovery.
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt)
## Model
[ApiResponse](../../app/src/main/java/com/alnafay/arzepak/model/ApiResponse.kt)
## Navigation
- **Navigate from**
  - [ManualLoginActivity](../../docs/activity/ManualLoginActivity.md)