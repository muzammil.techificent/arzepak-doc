# [VideoPlayerActivity](../../app/src/main/java/com/alnafay/arzepak/activities/videoPlayer/VideoPlayerActivity.kt)
## Introduction
Videos which are added at property submitting time will be shown here .
## Responsibility
- Show video with the following links
  - Vimeo
  - Dailymotion
  - Youtube
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Navigation
- **Navigate from**
  - [PropertyDetailActivity](../../docs/activity/PropertyDetailActivity.md)