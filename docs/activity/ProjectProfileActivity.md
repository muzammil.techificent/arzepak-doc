# [ProjectProfileActivity](../../app/src/main/java/com/alnafay/arzepak/activities/project/ProjectProfileActivity.kt)
## Introduction
This class will show the project detail.
## Responsibility
- Favourite the projects
- Share it
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[ProjectProfilePresenter](../../app/src/main/java/com/alnafay/arzepak/activities/project/ProjectProfilePresenter.kt)
## View
[ProjectProfileView](../../app/src/main/java/com/alnafay/arzepak/activities/project/ProjectProfileView.kt)
## Model
- [Attachment](../../app/src/main/java/com/alnafay/arzepak/model/Attachment.kt)
- [FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
- [PaymentPlan](../../app/src/main/java/com/alnafay/arzepak/model/project/PaymentPlan.kt)
- [Project.kt](../../app/src/main/java/com/alnafay/arzepak/model/project/Project.kt)

## Navigation
- **Navigate from**
  - [NewDiscoverFragment](../../docs/fragments/NewDiscoverFragment.md)
  - [ProjectsActivity](../../docs/activity/ProjectsActivity.md)
  - [ProjectFavouriteFragment](../fragments/ProjectFavouriteFragment.md)

