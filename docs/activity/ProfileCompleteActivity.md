# [ProfileCompleteActivity](../../app/src/main/java/com/alnafay/arzepak/activities/login/socialLogin/profileComplete/ProfileCompleteActivity.kt)
## Introduction
This class is basically phone number restricted means after logged in or sign up by Facebook or Gmail it appears to update user profile .
## Responsibility
- Update profile
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[ProfileCompletePresenter](../../app/src/main/java/com/alnafay/arzepak/activities/login/socialLogin/profileComplete/ProfileCompletePresenter.kt)
## View
[ProfileCompleteView](../../app/src/main/java/com/alnafay/arzepak/activities/login/socialLogin/profileComplete/ProfileCompleteView.kt)
## Model
[EditProfileRequest](../../app/src/main/java/com/alnafay/arzepak/model/request/EditProfileRequest.kt)
## Navigation
- **Navigate to**
  - [HomeActivity](../../docs/activity/HomeActivity.md)
  - [SocialLoginActivity](../../docs/activity/SocialLoginActivity.md)
- **Navigate from**
  - [SocialLoginActivity](../../docs/activity/SocialLoginActivity.md)
