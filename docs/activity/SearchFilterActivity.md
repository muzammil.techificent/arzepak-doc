# [SearchFilterActivity](../../app/src/main/java/com/alnafay/arzepak/activities/searchFilter/SearchFilterActivity.kt)
## Introduction
This class will take some input about property filter elements  and save to the filter model and send back to the screen where it is called from .
## Responsibility
- Shows all cities data
- Show property types
- Take min and max values of price
- Take min and max values of area
- Take number of baths as well as rooms
- Send them to [HomeActivity](../../docs/activity/HomeActivity.md)
## Parent Class

[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[SearchFilterPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/searchFilter/SearchFilterPresenter.kt)
## View
[SearchFilterView](../../app/src/main/java/com/alnafay/arzepak/activities/searchFilter/SearchFilterView.kt)
## Model
-  [PriceAreaRang](../../app/src/main/java/com/alnafay/arzepak/model/PriceAreaRang.kt)
-  [PropertyOption](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/PropertyOption.kt)
-  [AreaUnit](../../app/src/main/java/com/alnafay/arzepak/model/config/AreaUnit.kt)
-  [PropertyType](../../app/src/main/java/com/alnafay/arzepak/model/config/PropertyType.kt)
-  [SubPropertyType](../../app/src/main/java/com/alnafay/arzepak/model/config/SubPropertyType.kt)
-  [SearchQuery](../../app/src/main/java/com/alnafay/arzepak/model/search/SearchQuery.kt)
## Navigation
- **Navigate to**
  - [HomeActivity](../../docs/activity/HomeActivity.md)
- **Navigate from**
  - [HomeActivity](../../docs/activity/HomeActivity.md)