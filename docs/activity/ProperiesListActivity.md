# [PropertiesListActivity](../../app/src/main/java/com/alnafay/arzepak/activities/propertiesList/PropertiesListActivity.kt)
## Introduction
This will list down all the properties which are requested .
## Responsibility
- Get list of properties
- shows them all
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Model
- [Property](../../app/src/main/java/com/alnafay/arzepak/model/propertiesListing/Property.kt)
- [FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
- [AlgoliaResponse](../../app/src/main/java/com/alnafay/arzepak/model/AlgoliaResponse.kt)

## Navigation
- **Navigate to**
  - [PropertyDetailActivity](../../docs/activity/PropertyDetailActivity.md)
- **Navigate from**
  - [NewDiscoverFragment](../../docs/fragments/NewDiscoverFragment.md)
  - [PopularLocationActivity](../../docs/activity/PopularLocationActivity.md)
