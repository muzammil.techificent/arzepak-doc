# [RegistrationActivity](../../app/src/main/java/com/alnafay/arzepak/activities/registration/RegistrationActivity.kt)
## Introduction
This class will take input step by step and sign you up .
## Responsibility
- Takes name , email , phone number , gander , password and then sign you  up .
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[RegistrationPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/registration/RegistrationPresenter.kt)
## View
[RegistrationViewInterface](../../app/src/main/java/com/alnafay/arzepak/activities/registration/RegistrationViewInterface.kt)
## Model
[RegistrationRequest](../../app/src/main/java/com/alnafay/arzepak/model/request/RegistrationRequest.kt)
## Navigation
- **Navigate to**
  - [ManualLoginActivity](../../docs/activity/ManualLoginActivity.md)
  - [HomeActivity](../../docs/activity/HomeActivity.md)
- **Navigate from**
  - [ManualLoginActivity ](../../docs/activity/ManualLoginActivity.md)