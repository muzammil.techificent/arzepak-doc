# [SelectCityActivity](../../app/src/main/java/com/alnafay/arzepak/activities/selectCity/SelectCityActivity.kt)
## Introduction
There are many cities added in database and updated gradually to this class get these cities adn shows in list from .
## Responsibility
- Get cities list .
- Send selected city object to the class where from it is called .
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[SelectCityPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/selectCity/SelectCityPresenter.kt)
## View
[SelectCityView](../../app/src/main/java/com/alnafay/arzepak/activities/selectCity/SelectCityView.kt)
## Model
[City](../../app/src/main/java/com/alnafay/arzepak/model/config/City.kt)
## Navigation
- **Navigate to**
  - [AddPropertyFragmentStep2](../../docs/fragments/AddPropertyFragmentStep2.md)
  - [SearchFilterActivity](../../docs/activity/SearchFilterActivity.md)
- **Navigate from**
   - [AddPropertyFragmentStep2](../../docs/fragments/AddPropertyFragmentStep2.md)
  - [SearchFilterActivity](../../docs/activity/SearchFilterActivity.md)