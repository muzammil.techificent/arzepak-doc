# [ProjectsActivity](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/projects/ProjectsActivity.kt)
## Introduction
This class will show the project list.
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[ProjectsPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/projects/ProjectsPresenter.kt)
## View
[ProjectsView](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/projects/ProjectsView.kt)
## Model
[Project](../../app/src/main/java/com/alnafay/arzepak/model/project/Project.kt)
## Navigation
- **Navigate to**
  - [ProjectProfileActivity](../../docs/activity/ProjectProfileActivity.md)
- **Navigate from**
  - [NewDiscoverFragment](../../docs/fragments/NewDiscoverFragment.md)