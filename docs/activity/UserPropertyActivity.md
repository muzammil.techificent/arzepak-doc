# [UserPropertyActivity](../../app/src/main/java/com/alnafay/arzepak/activities/propertiesList/propertyListingByUser/UserPropertyActivity.kt)
## Introduction
This class shows all the property of particular user .
## Responsibility
- Get properties of particular user
- shows them all
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[UserPropertyPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/propertiesList/propertyListingByUser/UserPropertyPresenter.kt)
## View
[MyPropertyView](../../app/src/main/java/com/alnafay/arzepak/activities/propertiesList/propertyListingByUser/MyPropertyView.kt)
## Model
Property[Property](../../app/src/main/java/com/alnafay/arzepak/model/propertiesListing/Property.kt)
## Navigation
- **Navigate to**
  - [PropertyDetailActivity](../../docs/activity/PropertyDetailActivity.md)
- **Navigate from**
  - [AgencyActivity](../../docs/activity/AgencyActivity.md)