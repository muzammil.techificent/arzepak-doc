# [SearchActivity](../../app/src/main/java/com/alnafay/arzepak/activities/searchFilter/searchText/SearchActivity.kt)
## Introduction
This class will get list of property title on behalf of Query that is send to the server .
## Responsibility
- Send text to the server and get list of property title .
- Send you selected search to the [HomeFragment](../../docs/fragments/HomeFragment.md) where filter will perform.
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[SearchPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/searchFilter/searchText/SearchPresenter.kt)
## View
[SearchView](../../app/src/main/java/com/alnafay/arzepak/activities/searchFilter/searchText/SearchView.kt)
## Model
- [AlgoliaResponse](../../app/src/main/java/com/alnafay/arzepak/model/AlgoliaResponse.kt)
- [SearchLocation](../../app/src/main/java/com/alnafay/arzepak/model/search/SearchLocation.kt)
## Navigation
- **Navigate to**
  - [HomeFragment](../../docs/fragments/HomeFragment.md)
- **Navigate from**
  - [HomeFragment](../../docs/fragments/HomeFragment.md)