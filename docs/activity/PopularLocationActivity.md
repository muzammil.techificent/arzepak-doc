# [PopularLocationActivity](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/popularLocation/PopularLocationActivity.kt)
## Introduction
This will show the all popular location .
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[PopularLocationPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/popularLocation/PopularLocationPresenter.kt)
## View
[PopularLocationView](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/popularLocation/PopularLocationView.kt)
## Navigation
- **Navigate to**
  - [PropertiesListActivity](../../docs/activity/ProperiesListActivity.md)
- **Navigate from**
  - [NewDiscoverFragment](../../docs/fragments/NewDiscoverFragment.md)