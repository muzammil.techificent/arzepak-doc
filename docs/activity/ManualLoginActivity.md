# [ManualLoginActivity](../../app/src/main/java/com/alnafay/arzepak/activities/login/manualLogin/ManualLoginActivity.kt)
## Introduction
This class logged in as well as signed up the user by Facebook or Gmail .
## Responsibility
- Logged in by Facebook and Gmail
- Signed up by Facebook and Gmail
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[ManualLoginPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/login/manualLogin/ManualLoginPresenter.kt)
## View
[ManualLoginViewInterface](../../app/src/main/java/com/alnafay/arzepak/activities/login/manualLogin/ManualLoginViewInterface.kt)
## Navigation
- **Navigate to**
  - [HomeActivity](../../docs/activity/HomeActivity.md)
  - [ProfileCompleteActivity](../../docs/activity/ProfileCompleteActivity.md)
  - [RegistrationActivity](../../docs/activity/Registration.md)
- **Navigate from**
  - [RegistrationActivity](../../docs/activity/Registration.md)
  - [SocialLoginActivity](../../docs/activity/SocialLoginActivity.md)
