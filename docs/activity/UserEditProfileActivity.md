# [UserEditProfileActivity](../../app/src/main/java/com/alnafay/arzepak/activities/userEditProfile/UserEditProfileActivity.kt)
## Introduction
To change the User Credentials , this class helps to edit user profile .
## Responsibility
- Take image from **Gallery** as well as **Camera** and will update that to server.
- Edit user data ( Name , Phone number ).
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[UserEditProfilePresenter](../../app/src/main/java/com/alnafay/arzepak/activities/userEditProfile/UserEditProfilePresenter.kt)
## View
[UserEditProfileView](../../app/src/main/java/com/alnafay/arzepak/activities/userEditProfile/UserEditProfileView.kt)
## Model
- [EditProfileRequest](../../app/src/main/java/com/alnafay/arzepak/model/request/EditProfileRequest.kt)
## Navigation
- **Navigate from**
  - [SettingsActivity](../../docs/activity/SettingsActivity.md)