# [ChangePasswordActivity](../../app/src/main/java/com/alnafay/arzepak/activities/changePassword/ChangePasswordActivity.kt)
## Introduction
This Class is only responsible to change user password .
## Responsibility
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[ChangePasswordPresenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/changePassword/ChangePasswordPresenter.kt)
## View
[ChangePasswordViewInterface.kt](../../app/src/main/java/com/alnafay/arzepak/activities/changePassword/ChangePasswordViewInterface.kt)
- **Navigate from**
  - [SettingsActivity](../../docs/activity/SettingsActivity.md)
