## Base Activity
## Introduction [⤴]()
Base Activity is used as a Parent class of every activity , so the features or functionality which are consider to be same in more than one activities are implement in [Base Activity]() .  
As we know  , we are working with [MVP](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter)  so our pattern is  based on three classes (View Class  , Presenter Class and UI Class).
Here are some functions those were implemented in Base Class and these are ...  
<br/>

**Function** | **Params** | **Optional Params**|  **Using For**|
---|---|---|---
**Show Message** | Title <br> Message <br> Toast Type <br> **Note :** Toast type are (Error , Success , Info and Warning)|Time|Used for Custom Toast
**Change Status Bar Color** | Color <br> **Note :** Color can be in string form or integer form   |  |To change the color of the status bar|
**Launch Activity** | Class which need to be initiated <br> Boolean ( should finish current activity )  | Bundle |To Launch new activity|
**Launch Activity For Result** | Class which need to be initiated <br> Result code | Bundle |To launch new activity which will give some information after finishing.
**Hide Progress Bar** |  |  |To hide the progress bar when it is showing|
**Show Progress Bar** |  |  |To show progress bar when some task are running |
**Remove Status Bar** |  |  |There are some screens where we do not need status bar|
**Hide Keyboard** |   |  |To remove keyboard from screen specially when input has done|
**Select Image From Gallery Or Camera** |  |  |When we need to show gallery image or image from camera , we use this function which will return image on **OnActivityForResult** in bundle form.|
**Secure App Permissions** |   |   | used to handle multiple permission Handling   |
