# [AddPropertyActivity](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/AddPropertyActivity.kt)
## Introduction
This class represent Add Property as well as Edit Property.
## Responsibility
This class is has its sub class in fragment form which takes input and submit the property for the approval .<br>
here are sub classes ,
 - **[AddPropertyFragmentStep1](../../docs/fragments/AddPropertyFragmentStep1.md)**
 - **[AddPropertyFragmentStep2](../../docs/fragments/AddPropertyFragmentStep2.md)**
 - **[AddPropertyFragmentStep3](../../docs/fragments/AddPropertyFragmentStep3.md)**
 - **[AddPropertyPreviewFragment](../../docs/fragments/AddPropertyPreviewFragment.md)**
## Parent Class
 - [BasePresenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BasePresenter.kt)
## Presenter
 - [AddPropertyPresenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/AddPropertyPresenter.kt)
## View
 - [AddPropertyView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/AddPropertyView.kt)
## Model
 - [MediaAttachmentsItem](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/MediaAttachmentsItem.kt)
 - [RequestAddProperty](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/RequestAddProperty.kt)
 - [MediaFile](../../app/src/main/java/com/alnafay/arzepak/model/media/MediaFile.kt)
## Navgation
- **Navigate from**
  - [BaseDrawerActivity](../base/BaseDrawerActivity.md)
  - [MyPropertyActivity](MyPropertyActivity.md)
