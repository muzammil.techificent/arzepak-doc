# [AgenciesActivity](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/agencies/AgenciesActivity.kt)
## Introduction
This class will shows all agencies  .
## Responsibility
- Get agencies list
- Shows them  in list
## Parent Class
[BaseActivity](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[AgenciesActivity](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/agencies/AgenciesActivity.kt)
## View
[AgenciesView](../../app/src/main/java/com/alnafay/arzepak/activities/discoverActivities/agencies/AgenciesView.kt)
## Model
[FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
[Agency](../../app/src/main/java/com/alnafay/arzepak/model/agency/Agency.kt)
## Navigation
- **Navigate to**
  - [AgencyActivity](../../docs/activity/AgencyActivity.md)
- **Navigate from**
  - [NewDiscoverFragment](../../docs/fragments/NewDiscoverFragment.md)