# Base Drawer Activity
## Introduction
Activities which are using Drawer will extend this class .
## Responsibility
This class is responsible to navigate new screens . Can create custom UI and handle user login Preferences .
## Parent Class
[BaseActivity.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseActivity.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## View
[BaseDrawerView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/navDrawerActivity/BaseDrawerView.kt) extend with [BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Model
[User.kt](../../app/src/main/java/com/alnafay/arzepak/model/login/response/User.kt)
## Navigate
- **Navigate to**
  - [AddPropertyActivity](../../docs/activity/AddProperty.md)
  - [HomeActivity](../../docs/activity/HomeActivity.md)
  - [SocialLoginActivity](../../docs/activity/SocialLoginActivity.md)
  - [MyPropertyActivity](../../docs/activity/MyPropertyActivity.md)
  - [SettingsActivity](../../docs/activity/SettingsActivity.md)
  - [WebActivity](../../docs/activity/WebActivity.md)



