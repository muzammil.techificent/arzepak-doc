# BaseActivity
## Introduction
All activities will extends BaseActivity as it is handling some functionalities which are likely to be same in many activities.
## Responsibility
This class is responsible to handle application secure permission and some same functonalities over the app .
## Parent Class
[BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
## Presenter
[BasePresenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BasePresenter.kt)
## View
[BaseView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/BaseView.kt)
