# BaseActivity
## Introduction
All activities will extends BaseActivity as it is handling some functionalities which are likely to be same in many activities.
## Responsibility
This class is responsible to handle application secure permission and some same functonalities over the app .
## Parent Class
[BaseFragmentView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragmentView.kt)
## Presenter
[BaseFragmentPresenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragmentPresenter.kt)
## View
[BaseFragmentView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragmentView.kt)
