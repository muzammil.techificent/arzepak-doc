# [AddPropertyFragmentStep3](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_3/AddPropertyFragmentStep3.kt)
## Introduction
This class requires Property Media in images form and online youtube URL for video.
## Responsibility
Responsible to added media in Request Model .
## Parent Class
[BaseFragment.kt](../base/BaseFragment.md)
## Presenter
[AddPropertyFragmentStep3Presenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_3/AddPropertyFragmentStep3Presenter.kt)
## View
[AddPropertyFragmentStep3View.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_3/AddPropertyFragmentStep3View.kt)
## Model
- [RequestAddProperty.kt](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/RequestAddProperty.kt)
- [ApiResponse.kt](../../app/src/main/java/com/alnafay/arzepak/model/ApiResponse.kt)
- [MediaFile.kt](../../app/src/main/java/com/alnafay/arzepak/model/media/MediaFile.kt)
- [MediaAttachmentsItem.kt](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/MediaAttachmentsItem.kt)
## Navigation
- **Navigate to**
  - [AddPropertyPreviewFragment](../../docs/fragments/AddPropertyPreviewFragment.md)
- **Navigate from**
  - [AddPropertyFragmentStep2](../../docs/fragments/AddPropertyFragmentStep2.md)
  -  [AddPropertyPreviewFragment](../../docs/fragments/AddPropertyPreviewFragment.md)
