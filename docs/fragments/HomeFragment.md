
# [HomeFragment](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/home/HomeFragment.kt)
## Introduction
The first screen which appears after your authentications is Home Screen with properties which are listing around your location.
## Responsibility
- Filter property
- shows properties in list as well as pager form
- Take location and will show properties around location
- Shows properties within draw line (by finger on map)
- Shows customised map in ``satellite`` and `normal` form.
## Parent Class
[BaseFragment.kt](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragment.kt)
## Presenter
[HomeFragmentPresenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/home/HomeFragmentPresenter.kt)
## View
[HomeFragmentView.kt](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/home/HomeFragmentView.kt)
## Model
- [SearchQuery](../../app/src/main/java/com/alnafay/arzepak/model/search/SearchQuery.kt)
- [Property](../../app/src/main/java/com/alnafay/arzepak/model/propertiesListing/Property.kt)
- [FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
- [ClusterItem](../../app/src/main/java/com/alnafay/arzepak/model/ClusterItem.kt)
## Navigation
- **Navigate to**
  - [SearchActivity](../../docs/activity/SearchActivity.md)
  - [SearchFilterActivity](../../docs/activity/SearchFilterActivity.md)
- **Navigate from**
  - [Drawer](../../docs/base/BaseDrawerActivity.md)
