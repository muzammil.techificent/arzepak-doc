# [AddPropertyPreviewFragment](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyPreview/AddPropertyPreviewFragment.kt)
## Introduction
This class will show your property detail so that you can verify these data .
## Responsibility
- Show added data
## Parent Class
[BaseFragment](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragment.kt)
## Presenter
[AddPropertyPreviewFragmentPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyPreview/AddPropertyPreviewFragmentPresenter.kt)
## View
[AddPropertyPreviewFragmentView](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyPreview/AddPropertyPreviewFragmentView.kt)
## Model
 - [MediaAttachmentsItem](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/MediaAttachmentsItem.kt)
- [RequestAddProperty](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/RequestAddProperty.kt)
- [Amenity](../../app/src/main/java/com/alnafay/arzepak/model/config/Amenity.kt)
- [City](../../app/src/main/java/com/alnafay/arzepak/model/config/City.kt)
- [PropertyType](../../app/src/main/java/com/alnafay/arzepak/model/config/PropertyType.kt)
- [SubPropertyType](../../app/src/main/java/com/alnafay/arzepak/model/config/SubPropertyType.kt)
## Navigation
- **Navigate from**
  - [AddPropertyFragmentStep3](../../docs/fragments/AddPropertyFragmentStep3.md)