# [FavoriteFragment](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/FavoriteFragment.kt)
## Introduction
Properties can be Favourited by this class you just need to click on the heart and that property will appears here.
Here are three main fragment which are attached to it and they are :
- [PropertyFavouriteFragment](../../docs/fragments/PropertyFavouriteFragment.md)
- [ProjectFavouriteFragment](../../docs/fragments/ProjectFavouriteFragment.md)
- [AgencyFavouriteFragment](../../docs/fragments/AgencyFavouriteFragment.md)
## Responsibility
Responsibility is this class is to show all those fragment which are mentioned above.
## Parent Class
[BaseFragment](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragment.kt)
## Presenter
[FavoriteFragmentPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/FavoriteFragmentPresenter.kt)
## View
[FavoriteFragmentView](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/FavoriteFragmentView.kt)
## Navigation
- **Navigate from**
  - HomeActivity-

