# [AgencyFavouriteFragment](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/agencies/AgenciesFragment.kt)
## Introduction
This class is responsible to save the property object in data base which is [Room DataBase](https://developer.android.com/training/data-storage/room).  
No property will be added more than one time .
## Parent Class
[BaseFragment](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragment.kt)
## Presenter
[AgenciesFragmentPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/agencies/AgenciesFragmentPresenter.kt)
## View
[AgenciesFragmentView](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/agencies/AgenciesFragmentView.kt)
## Model
- [FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
- [Agency](../../app/src/main/java/com/alnafay/arzepak/model/agency/Agency.kt)
## Navigation
- **Navigate to**
  - [AgencyActivity](../activity/AgencyActivity.md)
