# [ProjectsFragment](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/projects/ProjectsFragment.kt)
## Introduction
This class is responsible to save the property object in data base which is [Room DataBase](https://developer.android.com/training/data-storage/room).  
No property will be added more than one time .
## Parent Class
[BaseFragment](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragment.kt)
## Presenter
[ProjectsFragmentPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/projects/ProjectsFragmentPresenter.kt)
## View
[ProjectsFragmentView](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/projects/ProjectsFragmentView.kt)
## Model
- [FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
- [Project](../../app/src/main/java/com/alnafay/arzepak/model/project/Project.kt)
## Navigation
- **Navigate to**
  - [ProjectProfileActivity](../activity/ProjectProfileActivity.md)
