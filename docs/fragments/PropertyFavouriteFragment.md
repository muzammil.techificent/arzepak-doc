# [PropertiesFragment](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/properties/PropertiesFragment.kt)
## Introduction
This class is responsible to save the property object in data base which is [Room DataBase](https://developer.android.com/training/data-storage/room).
No property will be added more than one time .
## Parent Class
[BaseFragment](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragment.kt)
## Presenter
[PropertiesFragmentPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/properties/PropertiesFragmentPresenter.kt)
## View
[PropertiesFragmentView](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/favorite/properties/PropertiesFragmentView.kt)
## Model
- [FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
- [Property](../../app/src/main/java/com/alnafay/arzepak/model/propertiesListing/Property.kt)
## Navigation
- **Navigate to**
  - [PropertyDetailActivity](../activity/PropertyDetailActivity.md)
