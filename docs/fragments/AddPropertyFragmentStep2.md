# [AddPropertyFragmentStep2](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_2/AddPropertyFragmentStep2.kt)
## Introduction
This class requires property detail added by the user  .
## Responsibility
Property detail which is added by user will be checked if wrongly input .
Responsible to get exact location from Google map , Amenities provided by Company etc .
## Parent Class
[BaseFragment.kt](../base/BaseFragment.md)
## Presenter
[AddPropertyFragmentStep2Presenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_2/AddPropertyFragmentStep2Presenter.kt)
## View
[AddPropertyFragmentStep2View.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_2/AddPropertyFragmentStep2View.kt)
## Model
- [RequestAddProperty.kt](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/RequestAddProperty.kt)
- [PropertyType.kt](../../app/src/main/java/com/alnafay/arzepak/model/config/PropertyType.kt)
- [SubPropertyType.kt](../../app/src/main/java/com/alnafay/arzepak/model/config/SubPropertyType.kt)
- [PropertyOption.kt](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/PropertyOption.kt)
- [Amenity.kt](../../app/src/main/java/com/alnafay/arzepak/model/config/Amenity.kt)
- [AreaUnit.kt](../../app/src/main/java/com/alnafay/arzepak/model/config/AreaUnit.kt)
- [City.kt](../../app/src/main/java/com/alnafay/arzepak/model/config/City.kt)
## Navigation
- **Navigate to**
  - [AddPropertyFragmentStep3.kt](../../docs/fragments/AddPropertyFragmentStep3.md)
- **Navigate from**
  - [AddPropertyFragmentStep1.kt](../../docs/fragments/AddPropertyFragmentStep1.md)
  - [AddPropertyFragmentStep3.kt](../../docs/fragments/AddPropertyFragmentStep3.md)
