# [NewDiscoverFragment](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/newDiscover/NewDiscoverFragment.kt)
## Introduction
This class displays latest and top ( Properties , Projects and Agencies ).
## Responsibility
- Display displays latest and top
  - Properties
  - Projects
  - Agencies
- Search
- Fav and Un-Fav
## Parent Class
[BaseFragment](../../app/src/main/java/com/alnafay/arzepak/activities/baseActivity/baseFragment/BaseFragment.kt)
## Presenter
[NewDiscoverPresenter](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/newDiscover/NewDiscoverPresenter.kt)
## View
[NewDiscoverView](../../app/src/main/java/com/alnafay/arzepak/activities/home/fragments/newDiscover/NewDiscoverView.kt)
## Model
- [Property](../../app/src/main/java/com/alnafay/arzepak/model/propertiesListing/Property.kt)
- [Project](../../app/src/main/java/com/alnafay/arzepak/model/project/Project.kt)
- [PopularLocationsCityVise](../../app/src/main/java/com/alnafay/arzepak/model/popularLocation/PopularLocationsCityVise.kt)
- [FavoriteEvents](../../app/src/main/java/com/alnafay/arzepak/model/eventBus/FavoriteEvents.kt)
- [Agency](../../app/src/main/java/com/alnafay/arzepak/model/agency/Agency.kt)
## Navigation
- **Navigate to**
  - [PropertyDetailActivity](../../docs/activity/PropertyDetailActivity.md)
  - [ProperiesListActivity](../../docs/activity/ProperiesListActivity.md)
  - [ProjectProfileActivity](../../docs/activity/ProjectProfileActivity.md)
  - [ProjectsActivity](../../docs/activity/ProjectsActivity.md)
  - [AgencyActivity](../../docs/activity/AgencyActivity.md)
  - [AgenciesActivity](../../docs/activity/AgenciesActivity.md)
- **Navigate from**
  - [HomeFragment](../../docs/fragments/HomeFragment.md)