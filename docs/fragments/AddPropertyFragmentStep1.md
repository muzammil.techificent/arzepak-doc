# [AddPropertyFragmentStep1](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_1/AddPropertyFragmentStep1.kt)
## Introduction
This class is basically a fragment which will manage the user input from the user and save it in model .
## Responsibility
This class is responsible to check user data
## Parent Class
[BaseFragment.kt](../base/BaseFragment.md)
## Presenter
[AddPropertyFragmentStep2Presenter.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_2/AddPropertyFragmentStep2Presenter.kt)
## View
[AddPropertyFragmentStep2View.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_2/AddPropertyFragmentStep2View.kt)
## Model
- [RequestAddProperty.kt](../../app/src/main/java/com/alnafay/arzepak/model/addProperty/RequestAddProperty.kt)
- [PropertyType.kt](../../app/src/main/java/com/alnafay/arzepak/model/config/PropertyType.kt)
- [SubPropertyType.kt](../../app/src/main/java/com/alnafay/arzepak/model/config/SubPropertyType.kt)
## Navigation
- **Navigate to**
  - [AddPropertyFragmentStep2.kt](../../docs/fragments/AddPropertyFragmentStep2.md)
- **Navigate from**
  - [AddPropertyFragmentStep2.kt](../../app/src/main/java/com/alnafay/arzepak/activities/addProperty/fragment/addPropertyFragmentStep_2/AddPropertyFragmentStep2.kt)