# Arzepak Android Application

## Table of contents

- [Introduction](#introduction-)
- [Development Tools and Languages](#development-tools-and-languages-)
- [Architecture and Design Pattern](#architecture-and-design-pattern-)
- [Coding style & standard](#coding-style--standard-)
  - [Directory structure](#directory-structure-)
  - [Naming Conventions](#naming-conventions-)
    - [Package naming](#packages-naming-)
    - [Classes, Interfaces and objects naming](#classes-interfaces-and-objects-naming-)
    - [Functions, properties and variables naming](#functions-properties-and-variables-naming-)
    - [Flags and Constants naming](#flags-and-constants-naming-)
- [Third party integrations](#third-party-integrations--licences-)
- [Contributors](#contributors-)
- [Licence](#licence-)

## Introduction [⤴](#table-of-contents)

[Arzepak](http://www.arzepak.com) Arzepak is a digital real estate marketplace where you can buy, rent and sell property. Users would now be able to search a wide range of properties for sale and to rent from estate agents, developers, banks and homeowners. From first time buyers to experienced investors, Arzepak will help everyone find the property they require.

## Development Tools and Languages [⤴](#table-of-contents)

arzepak android application is fully build on **[Kotlin](https://kotlinlang.org/docs/reference/)** Language on Android Studio 

## Architecture and Design Pattern [⤴](#table-of-contents)

arzepak application is develop on **[MVP](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter)** (Model-view-presenter) architecture as 
base architecture, while in whole app you can witness some other design Pattern as well like Singleton, Adapter, Builder and etc.

## Coding style & standard [⤴](#table-of-contents)

this project following [Google’s Android coding standards](https://developer.android.com/kotlin/style-guide)

#### Directory structure [⤴](#table-of-contents)

all project files are in `com.alnafay.arzepak` package in their respected category package that are mention bellow and all packages naming has [`camel case`](https://en.wikipedia.org/wiki/Camel_case) notation

> note: `com.alnafay.arzepak` is **main** package

+ `activities` package contains subpackages by the name of the activity e.g. (`addProperty` package contain all `views`, `presenter` and `adapters` or some other classes that are only useful for __add property__ module)
+ every activity package contains package named `fragments` which contains all sub screen or module of that activity if it has sub screen or modules
+ `fragments` package has same structure as `activities`
+ `adapters` package contains base adapter or generic adapter that are being used by more then one screen
+ `customUI` package contains all UI components that are modified according to project need
+ `fcm` package contains all classes, services and utils that are require for Firebase push notifications
+ `interface` package contains all generic delegates 
+ `localDataHandler` package contains all DAO(Data access Object), Database, controller classes and process completion delegate for [Room](https://developer.android.com/training/data-storage/room) Local Database 
+ `model` package contains all data models 
+ `network` package contains all network calls controller and their callbacks delegate
+ `services` package contains all silent services that are being used in project
+ `sharedPreference` package contains all shared preferences handler utils of projects
+ `utils` contains all constants, Flags, Parsers functions, utilities functions, extension methods and etc.

#### Naming Conventions [⤴](#table-of-contents)

###### Source file names [⤴](#table-of-contents)

the whole project __Source Files__ use the [`camel case`](https://en.wikipedia.org/wiki/Camel_case) notation with an uppercase first letter (for example, `SharedPreferenceUtils.kt`)

###### Packages naming [⤴](#table-of-contents)

Names of packages are always lower case and did not use underscores (`com.alnafay.arzepak`). Using multi-word names is generally discouraged, but if required to use multiple words, simply concatenate them together or use the [`camel case`](https://en.wikipedia.org/wiki/Camel_case) (`com.alnafay.arzepak.sharedPreference`)

###### Classes, Interfaces and objects naming [⤴](#table-of-contents)

Names of classes, interfaces and objects start with an upper case letter and use the camel case:

```
class User { /*...*/ }
​
object Utility { /*...*/ }

interface Shareable { /*...*/ }
```

###### Functions, properties and variables naming [⤴](#table-of-contents)

Names of functions, properties and local variables start with a lower case letter and use the [`camel case`](https://en.wikipedia.org/wiki/Camel_case) and no underscores:

```
fun processDeclarations() { /*...*/ }

var declarationCount = 1

val mutableCollection: MutableSet<String> = HashSet()
```

###### Flags and Constants naming [⤴](#table-of-contents)

Names of constants or Flags (properties marked with const val, or top-level const val properties) should use uppercase underscore-separated names:
 
```
const val MAX_COUNT = 8

const val USER_NAME_FIELD = "UserName"
```

For enum constants, it's OK to use either uppercase underscore-separated names

```
enum class Color { RED, GREEN }
```

## Third party integrations & licences [⤴](#table-of-contents)

This software using following third party libraries/software/SDKs, Special thanks to following third party libraries and SDK for their contributions, [click here](third_party_licences.md) for their licences

1. [Retrofit](https://square.github.io/retrofit/) for rest API calls
2. [Gson](https://github.com/google/gson) for serialization/deserialization java/kotlin object into JSON
3. [Facebook Login SDK](https://developers.facebook.com/docs/facebook-login/android)
4. [ScrollingPagerIndicator](https://github.com/TinkoffCreditSystems/ScrollingPagerIndicator)
5. [Auto Fit TextView](https://github.com/grantland/android-autofittextview) auto adjustable text size textView
6. [Selectable Rounded ImageView](https://github.com/pungrue26/SelectableRoundedImageView) for rounded corner imageView
7. [Google Map SDK](https://developers.google.com/maps/documentation/android-sdk/intro) including google places, location and other map utils
8. [Firebase Crashlytics SDK](https://firebase.google.com/docs/crashlytics) for crash reporting (support)
9. [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging) for push notifications
10. [Picasso](https://square.github.io/picasso/) for image loading
11. [Glide](https://bumptech.github.io/glide/) for Gif image loading
12. [Chips Layout Manager](https://github.com/BelooS/ChipsLayoutManager) for chips listing
13. [Android Segmented Control](https://github.com/Kaopiz/android-segmented-control)
14. [Image Cropper](https://github.com/ArthurHub/Android-Image-Cropper)
15. [Android Room Database Architecture](https://developer.android.com/topic/libraries/architecture/room)
16. [Algolia Android SDK](https://www.algolia.com/doc/api-client/getting-started/install/android/)
17. [Event Bus](https://greenrobot.org/eventbus/) for in app data communication
18. [Crystal Range Seekbar](https://github.com/syedowaisali/crystal-range-seekbar)
19. [Facebook Shimmer Effect](https://facebook.github.io/shimmer-android/)
20. [Fancy Show Case View](https://github.com/faruktoptas/FancyShowCaseView)

## Contributors [⤴](#table-of-contents)

- [x] Muhammad Muzammil Sharif <muzammil@alnafay.com> as Senior Mobile Application Developer
- [x] Talha Bilal <talha.bilal@alnafay.com> as Senior Android Application Developer

## Licence [⤴](#table-of-contents)

>##### Arzepak Android Application
>
>is licenced under [MIT Licence](https://opensource.org/licenses/MIT)
>
> Copyright &copy; 2019-2020, [ALNAFAY IT SOLUTION (pvt)](http://www.alnafay.com).
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.