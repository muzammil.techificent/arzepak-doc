This software contains the following third party software.


### [Retrofit](https://square.github.io/retrofit/)

>Copyright 2013 Square, Inc.
>
>Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at
>
>   <http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.



### [Gson](https://github.com/google/gson)

>Copyright 2008 Google Inc.
>
>Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at
>
>    <http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.



### [Facebook Login SDK](https://github.com/facebook/facebook-android-sdk)

>Except as otherwise noted, the Facebook SDK for Android is licensed   
under the Facebook Platform License
>
><https://github.com/facebook/facebook-android-sdk/blob/master/LICENSE.txt>
>
>Unless required by applicable law or agreed to in writing, software   
distributed under the License is distributed on an "AS IS" BASIS,   
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   
See the License for the specific language governing permissions and   
limitations under the License.



### [ScrollingPagerIndicator](https://github.com/TinkoffCreditSystems/ScrollingPagerIndicator)

>Copyright 2018 Tinkoff Bank
>
>Licensed under the Apache License, Version 2.0 (the "License");  
>you may not use this file except in compliance with the License.  
>You may obtain a copy of the License at
>
>   http://www.apache.org/licenses/LICENSE-2.0
>
>Unless required by applicable law or agreed to in writing, software  
>distributed under the License is distributed on an "AS IS" BASIS,  
>WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
>See the License for the specific language governing permissions and  
>limitations under the License.



### [AutoFitTextView](https://github.com/grantland/android-autofittextview)

>Copyright 2014 Grantland Chew
>
>Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at
>
>   <http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.



### [SelectableRoundedImageView](https://github.com/pungrue26/SelectableRoundedImageView)

>Copyright 2014 Joonho Kim
>
>Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at
>
>   <http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.



### [Google map SDK](https://developers.google.com/maps/documentation/android-sdk/get-api-key)

> <https://cloud.google.com/maps-platform/terms>



### [Firebase Crashlytics SDK](https://firebase.google.com/docs/crashlytics)

><https://firebase.google.com/terms/crashlytics>



### [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging)

><https://firebase.google.com/terms/data-processing-terms>



### [Picasso](https://square.github.io/picasso/) 

>Copyright 2013 Square, Inc.
>
>Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at
>
> <http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.



### [Glide](https://bumptech.github.io/glide/)

>Copyright 2014 Google, Inc. All rights reserved.
>
>Redistribution and use in source and binary forms, with or without modification, are  
permitted provided that the following conditions are met:
>
>1. Redistributions of source code must retain the above copyright notice, this list of  
         conditions and the following disclaimer.
>
>2. Redistributions in binary form must reproduce the above copyright notice, this list  
of conditions and the following disclaimer in the documentation and/or other materials  
provided with the distribution.
>
>THIS SOFTWARE IS PROVIDED BY GOOGLE, INC. ``AS IS'' AND ANY EXPRESS OR IMPLIED  
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND  
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL GOOGLE, INC. OR  
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR  
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR  
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON  
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING  
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF  
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
>
>The views and conclusions contained in the software and documentation are those of the  
authors and should not be interpreted as representing official policies, either expressed  
or implied, of Google, Inc.



### [ChipsLayoutManager](https://github.com/BelooS/ChipsLayoutManager)

>Copyright 2016 Beloy Oleg, Ukraine.
>
>Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at
>
><http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.



### [Android Segmented Control](https://github.com/Kaopiz/android-segmented-control)

>The MIT License (MIT)
>
>Copyright (c) 2014 Le Van Hoang
>
>Permission is hereby granted, free of charge, to any person obtaining a copy  
of this software and associated documentation files (the "Software"), to deal  
in the Software without restriction, including without limitation the rights  
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  
copies of the Software, and to permit persons to whom the Software is  
furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in all  
copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  
SOFTWARE.



### [Android Image Cropper](https://github.com/ArthurHub/Android-Image-Cropper)

>Originally forked from edmodo/cropper.
>
>Copyright 2016, Arthur Teplitzki, 2013, Edmodo, Inc.
>
>Licensed under the Apache License, Version 2.0 (the "License");   
you may not use this work except in compliance with the License.   
You may obtain a copy of the License in the LICENSE file, or at:
>
><http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing,   
software distributed under the License is distributed on an   
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,   
either express or implied. See the License for the specific   
language governing permissions and limitations under the License.



### [Android Room (Local Database Architecture)](https://developer.android.com/topic/libraries/architecture/room)

><https://developer.android.com/topic/libraries/architecture/room>



### [Algolia Android SDK](https://www.algolia.com/doc/api-client/getting-started/install/android/)

>The MIT License (MIT)
>
>Copyright (c) 2013 Algolia
>
>Permission is hereby granted, free of charge, to any person obtaining a copy  
of this software and associated documentation files (the "Software"), to deal  
in the Software without restriction, including without limitation the rights  
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  
copies of the Software, and to permit persons to whom the Software is  
furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in all  
copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  
SOFTWARE.



### [Event Bus](https://greenrobot.org/eventbus/)

>Copyright (C) 2012-2020 Markus Junginger, greenrobot (<https://greenrobot.org>)
>
>EventBus binaries and source code can be used according to the Apache License, Version 2.0.



### [Crystal Range Seekbar](https://github.com/syedowaisali/crystal-range-seekbar)

>Licensed under the Apache License, Version 2.0 (the "License");   
you may not use this file except in compliance with the License.   
You may obtain a copy of the License at:
>
><http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing,   
software distributed under the License is distributed on an   
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,   
either express or implied. See the License for the specific   
language governing permissions and limitations under the License.
>
>##### Authors
>
> - Syed Owais Ali original Author
> - Balazs Rozsenich



### [Facebook Shimmer Effect](https://facebook.github.io/shimmer-android/)

>BSD License
>
>For Shimmer software
>
>Copyright (c) 2015, Facebook, Inc. All rights reserved.
>
>Redistribution and use in source and binary forms, with or without   
modification, are permitted provided that the following conditions are met:
>
>* Redistributions of source code must retain the above copyright notice,  
this list of conditions and the following disclaimer.
>
>* Redistributions in binary form must reproduce the above copyright   
notice, this list of conditions and the following disclaimer in the   
documentation and/or other materials provided with the distribution.
>
>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS   
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,   
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND   
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL   
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,   
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES   
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR   
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)   
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,   
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING   
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   
POSSIBILITY OF SUCH DAMAGE.



### [Fancy Show Case View](https://github.com/faruktoptas/FancyShowCaseView)

>Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at
>
> <http://www.apache.org/licenses/LICENSE-2.0>
>
>Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.